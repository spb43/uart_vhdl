-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 18.0.0 Build 614 04/24/2018 SJ Lite Edition"

-- DATE "07/21/2018 13:45:51"

-- 
-- Device: Altera 10M50DAF484C6GES Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	hard_block IS
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic
	);
END hard_block;

-- Design Ports Information
-- ~ALTERA_TMS~	=>  Location: PIN_H2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TCK~	=>  Location: PIN_G2,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDI~	=>  Location: PIN_L4,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_TDO~	=>  Location: PIN_M5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_CONFIG_SEL~	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- ~ALTERA_nCONFIG~	=>  Location: PIN_H9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_nSTATUS~	=>  Location: PIN_G9,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default
-- ~ALTERA_CONF_DONE~	=>  Location: PIN_F8,	 I/O Standard: 2.5 V Schmitt Trigger,	 Current Strength: Default


ARCHITECTURE structure OF hard_block IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \~ALTERA_TMS~~padout\ : std_logic;
SIGNAL \~ALTERA_TCK~~padout\ : std_logic;
SIGNAL \~ALTERA_TDI~~padout\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~padout\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~padout\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~padout\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~padout\ : std_logic;
SIGNAL \~ALTERA_TMS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TCK~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_TDI~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONFIG_SEL~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nCONFIG~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_nSTATUS~~ibuf_o\ : std_logic;
SIGNAL \~ALTERA_CONF_DONE~~ibuf_o\ : std_logic;

BEGIN

ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
END structure;


LIBRARY ALTERA;
LIBRARY FIFTYFIVENM;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE FIFTYFIVENM.FIFTYFIVENM_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	uart IS
    PORT (
	i_clk : IN std_logic;
	i_rst : IN std_logic;
	i_rx : IN std_logic;
	o_tx : OUT std_logic;
	KEY : IN std_logic_vector(1 DOWNTO 0);
	LEDR : OUT std_logic_vector(9 DOWNTO 0);
	SW : IN std_logic_vector(9 DOWNTO 0)
	);
END uart;

-- Design Ports Information
-- i_rst	=>  Location: PIN_R11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- i_rx	=>  Location: PIN_M20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- o_tx	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[1]	=>  Location: PIN_A7,	 I/O Standard: 3.3 V Schmitt Trigger,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_A8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[1]	=>  Location: PIN_A9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[2]	=>  Location: PIN_A10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[3]	=>  Location: PIN_B10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[4]	=>  Location: PIN_D13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[5]	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[6]	=>  Location: PIN_E14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[7]	=>  Location: PIN_D14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[8]	=>  Location: PIN_A11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- LEDR[9]	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- SW[8]	=>  Location: PIN_B14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_F15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- i_clk	=>  Location: PIN_M8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_B8,	 I/O Standard: 3.3 V Schmitt Trigger,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_C10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_C12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_A14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF uart IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_i_clk : std_logic;
SIGNAL ww_i_rst : std_logic;
SIGNAL ww_i_rx : std_logic;
SIGNAL ww_o_tx : std_logic;
SIGNAL ww_KEY : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_SW : std_logic_vector(9 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC1~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_ADC2~_CHSEL_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \i_clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \i_rst~input_o\ : std_logic;
SIGNAL \i_rx~input_o\ : std_logic;
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \~QUARTUS_CREATED_UNVM~~busy\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC1~~eoc\ : std_logic;
SIGNAL \~QUARTUS_CREATED_ADC2~~eoc\ : std_logic;
SIGNAL \o_tx~output_o\ : std_logic;
SIGNAL \LEDR[0]~output_o\ : std_logic;
SIGNAL \LEDR[1]~output_o\ : std_logic;
SIGNAL \LEDR[2]~output_o\ : std_logic;
SIGNAL \LEDR[3]~output_o\ : std_logic;
SIGNAL \LEDR[4]~output_o\ : std_logic;
SIGNAL \LEDR[5]~output_o\ : std_logic;
SIGNAL \LEDR[6]~output_o\ : std_logic;
SIGNAL \LEDR[7]~output_o\ : std_logic;
SIGNAL \LEDR[8]~output_o\ : std_logic;
SIGNAL \LEDR[9]~output_o\ : std_logic;
SIGNAL \i_clk~input_o\ : std_logic;
SIGNAL \i_clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \tx_module_maping|index~0_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[0]~13_combout\ : std_logic;
SIGNAL \tx_module_maping|LessThan0~0_combout\ : std_logic;
SIGNAL \tx_module_maping|LessThan0~1_combout\ : std_logic;
SIGNAL \tx_module_maping|LessThan0~2_combout\ : std_logic;
SIGNAL \tx_module_maping|LessThan0~3_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[0]~14\ : std_logic;
SIGNAL \tx_module_maping|prescaller[1]~15_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[1]~16\ : std_logic;
SIGNAL \tx_module_maping|prescaller[2]~17_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[2]~18\ : std_logic;
SIGNAL \tx_module_maping|prescaller[3]~19_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[3]~20\ : std_logic;
SIGNAL \tx_module_maping|prescaller[4]~21_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[4]~22\ : std_logic;
SIGNAL \tx_module_maping|prescaller[5]~23_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[5]~24\ : std_logic;
SIGNAL \tx_module_maping|prescaller[6]~25_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[6]~26\ : std_logic;
SIGNAL \tx_module_maping|prescaller[7]~27_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[7]~28\ : std_logic;
SIGNAL \tx_module_maping|prescaller[8]~29_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[8]~30\ : std_logic;
SIGNAL \tx_module_maping|prescaller[9]~31_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[9]~32\ : std_logic;
SIGNAL \tx_module_maping|prescaller[10]~33_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[10]~34\ : std_logic;
SIGNAL \tx_module_maping|prescaller[11]~35_combout\ : std_logic;
SIGNAL \tx_module_maping|prescaller[11]~36\ : std_logic;
SIGNAL \tx_module_maping|prescaller[12]~37_combout\ : std_logic;
SIGNAL \tx_module_maping|Equal0~1_combout\ : std_logic;
SIGNAL \tx_module_maping|Equal0~2_combout\ : std_logic;
SIGNAL \tx_module_maping|Equal0~0_combout\ : std_logic;
SIGNAL \tx_module_maping|Equal0~3_combout\ : std_logic;
SIGNAL \tx_module_maping|index[1]~1_combout\ : std_logic;
SIGNAL \tx_module_maping|index~3_combout\ : std_logic;
SIGNAL \tx_module_maping|index~2_combout\ : std_logic;
SIGNAL \tx_module_maping|index~4_combout\ : std_logic;
SIGNAL \tx_module_maping|f_tx_busy~0_combout\ : std_logic;
SIGNAL \tx_module_maping|f_tx_busy~1_combout\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \s_tx_data[4]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|s_tx_data[5]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|tx_process~0_combout\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \s_tx_data[3]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|Mux0~3_combout\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \s_tx_data[5]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|s_tx_data[6]~feeder_combout\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \s_tx_data[6]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|Mux0~4_combout\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \s_tx_data[7]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|s_tx_data[8]~feeder_combout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \s_tx_data[0]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|Mux0~1_combout\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \s_tx_data[2]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|s_tx_data[3]~feeder_combout\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \s_tx_data[1]~feeder_combout\ : std_logic;
SIGNAL \tx_module_maping|Mux0~0_combout\ : std_logic;
SIGNAL \tx_module_maping|Mux0~2_combout\ : std_logic;
SIGNAL \tx_module_maping|Mux0~5_combout\ : std_logic;
SIGNAL \tx_module_maping|o_tx_data~q\ : std_logic;
SIGNAL \tx_module_process~0_combout\ : std_logic;
SIGNAL \s_en_tx~feeder_combout\ : std_logic;
SIGNAL \s_en_tx~q\ : std_logic;
SIGNAL \tx_module_maping|f_tx_busy~q\ : std_logic;
SIGNAL \LEDR[0]~reg0feeder_combout\ : std_logic;
SIGNAL \LEDR[0]~reg0_q\ : std_logic;
SIGNAL \LEDR[1]~reg0feeder_combout\ : std_logic;
SIGNAL \LEDR[1]~reg0_q\ : std_logic;
SIGNAL \LEDR[2]~reg0feeder_combout\ : std_logic;
SIGNAL \LEDR[2]~reg0_q\ : std_logic;
SIGNAL \LEDR[3]~reg0feeder_combout\ : std_logic;
SIGNAL \LEDR[3]~reg0_q\ : std_logic;
SIGNAL \LEDR[4]~reg0_q\ : std_logic;
SIGNAL \LEDR[5]~reg0feeder_combout\ : std_logic;
SIGNAL \LEDR[5]~reg0_q\ : std_logic;
SIGNAL \LEDR[6]~reg0feeder_combout\ : std_logic;
SIGNAL \LEDR[6]~reg0_q\ : std_logic;
SIGNAL \LEDR[7]~reg0feeder_combout\ : std_logic;
SIGNAL \LEDR[7]~reg0_q\ : std_logic;
SIGNAL \tx_module_maping|prescaller\ : std_logic_vector(12 DOWNTO 0);
SIGNAL \tx_module_maping|index\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \tx_module_maping|s_tx_data\ : std_logic_vector(9 DOWNTO 0);
SIGNAL s_tx_data : std_logic_vector(7 DOWNTO 0);
SIGNAL \tx_module_maping|ALT_INV_f_tx_busy~q\ : std_logic;

COMPONENT hard_block
    PORT (
	devoe : IN std_logic;
	devclrn : IN std_logic;
	devpor : IN std_logic);
END COMPONENT;

BEGIN

ww_i_clk <= i_clk;
ww_i_rst <= i_rst;
ww_i_rx <= i_rx;
o_tx <= ww_o_tx;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
ww_SW <= SW;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\~QUARTUS_CREATED_ADC1~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\~QUARTUS_CREATED_ADC2~_CHSEL_bus\ <= (\~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\ & \~QUARTUS_CREATED_GND~I_combout\);

\i_clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \i_clk~input_o\);
\tx_module_maping|ALT_INV_f_tx_busy~q\ <= NOT \tx_module_maping|f_tx_busy~q\;
auto_generated_inst : hard_block
PORT MAP (
	devoe => ww_devoe,
	devclrn => ww_devclrn,
	devpor => ww_devpor);

-- Location: LCCOMB_X44_Y41_N16
\~QUARTUS_CREATED_GND~I\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \~QUARTUS_CREATED_GND~I_combout\ = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	combout => \~QUARTUS_CREATED_GND~I_combout\);

-- Location: IOOBUF_X78_Y49_N2
\o_tx~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \tx_module_maping|f_tx_busy~q\,
	devoe => ww_devoe,
	o => \o_tx~output_o\);

-- Location: IOOBUF_X46_Y54_N2
\LEDR[0]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[0]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[0]~output_o\);

-- Location: IOOBUF_X46_Y54_N23
\LEDR[1]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[1]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[1]~output_o\);

-- Location: IOOBUF_X51_Y54_N16
\LEDR[2]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[2]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[2]~output_o\);

-- Location: IOOBUF_X46_Y54_N9
\LEDR[3]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[3]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[3]~output_o\);

-- Location: IOOBUF_X56_Y54_N30
\LEDR[4]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[4]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[4]~output_o\);

-- Location: IOOBUF_X58_Y54_N23
\LEDR[5]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[5]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[5]~output_o\);

-- Location: IOOBUF_X66_Y54_N23
\LEDR[6]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[6]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[6]~output_o\);

-- Location: IOOBUF_X56_Y54_N9
\LEDR[7]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \LEDR[7]~reg0_q\,
	devoe => ww_devoe,
	o => \LEDR[7]~output_o\);

-- Location: IOOBUF_X51_Y54_N9
\LEDR[8]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[8]~output_o\);

-- Location: IOOBUF_X49_Y54_N9
\LEDR[9]~output\ : fiftyfivenm_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[9]~output_o\);

-- Location: IOIBUF_X0_Y18_N15
\i_clk~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_i_clk,
	o => \i_clk~input_o\);

-- Location: CLKCTRL_G3
\i_clk~inputclkctrl\ : fiftyfivenm_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \i_clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \i_clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X47_Y51_N18
\tx_module_maping|index~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|index~0_combout\ = (!\tx_module_maping|index\(3) & (\tx_module_maping|index\(1) $ (\tx_module_maping|index\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010101010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(3),
	datac => \tx_module_maping|index\(1),
	datad => \tx_module_maping|index\(0),
	combout => \tx_module_maping|index~0_combout\);

-- Location: LCCOMB_X46_Y51_N2
\tx_module_maping|prescaller[0]~13\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[0]~13_combout\ = \tx_module_maping|prescaller\(0) $ (VCC)
-- \tx_module_maping|prescaller[0]~14\ = CARRY(\tx_module_maping|prescaller\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \tx_module_maping|prescaller\(0),
	datad => VCC,
	combout => \tx_module_maping|prescaller[0]~13_combout\,
	cout => \tx_module_maping|prescaller[0]~14\);

-- Location: LCCOMB_X46_Y51_N0
\tx_module_maping|LessThan0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|LessThan0~0_combout\ = (!\tx_module_maping|prescaller\(3) & (((!\tx_module_maping|prescaller\(0)) # (!\tx_module_maping|prescaller\(1))) # (!\tx_module_maping|prescaller\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000011100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(2),
	datab => \tx_module_maping|prescaller\(1),
	datac => \tx_module_maping|prescaller\(3),
	datad => \tx_module_maping|prescaller\(0),
	combout => \tx_module_maping|LessThan0~0_combout\);

-- Location: LCCOMB_X46_Y51_N28
\tx_module_maping|LessThan0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|LessThan0~1_combout\ = (\tx_module_maping|prescaller\(6) & ((\tx_module_maping|prescaller\(5)) # ((\tx_module_maping|prescaller\(4) & !\tx_module_maping|LessThan0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000011100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(4),
	datab => \tx_module_maping|prescaller\(5),
	datac => \tx_module_maping|prescaller\(6),
	datad => \tx_module_maping|LessThan0~0_combout\,
	combout => \tx_module_maping|LessThan0~1_combout\);

-- Location: LCCOMB_X47_Y51_N28
\tx_module_maping|LessThan0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|LessThan0~2_combout\ = (\tx_module_maping|prescaller\(7)) # ((\tx_module_maping|prescaller\(8)) # ((\tx_module_maping|prescaller\(9)) # (\tx_module_maping|LessThan0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(7),
	datab => \tx_module_maping|prescaller\(8),
	datac => \tx_module_maping|prescaller\(9),
	datad => \tx_module_maping|LessThan0~1_combout\,
	combout => \tx_module_maping|LessThan0~2_combout\);

-- Location: LCCOMB_X46_Y51_N30
\tx_module_maping|LessThan0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|LessThan0~3_combout\ = (\tx_module_maping|prescaller\(12) & ((\tx_module_maping|prescaller\(11)) # ((\tx_module_maping|prescaller\(10) & \tx_module_maping|LessThan0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110000011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(10),
	datab => \tx_module_maping|prescaller\(11),
	datac => \tx_module_maping|prescaller\(12),
	datad => \tx_module_maping|LessThan0~2_combout\,
	combout => \tx_module_maping|LessThan0~3_combout\);

-- Location: FF_X46_Y51_N3
\tx_module_maping|prescaller[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[0]~13_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(0));

-- Location: LCCOMB_X46_Y51_N4
\tx_module_maping|prescaller[1]~15\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[1]~15_combout\ = (\tx_module_maping|prescaller\(1) & (!\tx_module_maping|prescaller[0]~14\)) # (!\tx_module_maping|prescaller\(1) & ((\tx_module_maping|prescaller[0]~14\) # (GND)))
-- \tx_module_maping|prescaller[1]~16\ = CARRY((!\tx_module_maping|prescaller[0]~14\) # (!\tx_module_maping|prescaller\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(1),
	datad => VCC,
	cin => \tx_module_maping|prescaller[0]~14\,
	combout => \tx_module_maping|prescaller[1]~15_combout\,
	cout => \tx_module_maping|prescaller[1]~16\);

-- Location: FF_X46_Y51_N5
\tx_module_maping|prescaller[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[1]~15_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(1));

-- Location: LCCOMB_X46_Y51_N6
\tx_module_maping|prescaller[2]~17\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[2]~17_combout\ = (\tx_module_maping|prescaller\(2) & (\tx_module_maping|prescaller[1]~16\ $ (GND))) # (!\tx_module_maping|prescaller\(2) & (!\tx_module_maping|prescaller[1]~16\ & VCC))
-- \tx_module_maping|prescaller[2]~18\ = CARRY((\tx_module_maping|prescaller\(2) & !\tx_module_maping|prescaller[1]~16\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \tx_module_maping|prescaller\(2),
	datad => VCC,
	cin => \tx_module_maping|prescaller[1]~16\,
	combout => \tx_module_maping|prescaller[2]~17_combout\,
	cout => \tx_module_maping|prescaller[2]~18\);

-- Location: FF_X46_Y51_N7
\tx_module_maping|prescaller[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[2]~17_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(2));

-- Location: LCCOMB_X46_Y51_N8
\tx_module_maping|prescaller[3]~19\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[3]~19_combout\ = (\tx_module_maping|prescaller\(3) & (!\tx_module_maping|prescaller[2]~18\)) # (!\tx_module_maping|prescaller\(3) & ((\tx_module_maping|prescaller[2]~18\) # (GND)))
-- \tx_module_maping|prescaller[3]~20\ = CARRY((!\tx_module_maping|prescaller[2]~18\) # (!\tx_module_maping|prescaller\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(3),
	datad => VCC,
	cin => \tx_module_maping|prescaller[2]~18\,
	combout => \tx_module_maping|prescaller[3]~19_combout\,
	cout => \tx_module_maping|prescaller[3]~20\);

-- Location: FF_X46_Y51_N9
\tx_module_maping|prescaller[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[3]~19_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(3));

-- Location: LCCOMB_X46_Y51_N10
\tx_module_maping|prescaller[4]~21\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[4]~21_combout\ = (\tx_module_maping|prescaller\(4) & (\tx_module_maping|prescaller[3]~20\ $ (GND))) # (!\tx_module_maping|prescaller\(4) & (!\tx_module_maping|prescaller[3]~20\ & VCC))
-- \tx_module_maping|prescaller[4]~22\ = CARRY((\tx_module_maping|prescaller\(4) & !\tx_module_maping|prescaller[3]~20\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(4),
	datad => VCC,
	cin => \tx_module_maping|prescaller[3]~20\,
	combout => \tx_module_maping|prescaller[4]~21_combout\,
	cout => \tx_module_maping|prescaller[4]~22\);

-- Location: FF_X46_Y51_N11
\tx_module_maping|prescaller[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[4]~21_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(4));

-- Location: LCCOMB_X46_Y51_N12
\tx_module_maping|prescaller[5]~23\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[5]~23_combout\ = (\tx_module_maping|prescaller\(5) & (!\tx_module_maping|prescaller[4]~22\)) # (!\tx_module_maping|prescaller\(5) & ((\tx_module_maping|prescaller[4]~22\) # (GND)))
-- \tx_module_maping|prescaller[5]~24\ = CARRY((!\tx_module_maping|prescaller[4]~22\) # (!\tx_module_maping|prescaller\(5)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(5),
	datad => VCC,
	cin => \tx_module_maping|prescaller[4]~22\,
	combout => \tx_module_maping|prescaller[5]~23_combout\,
	cout => \tx_module_maping|prescaller[5]~24\);

-- Location: FF_X46_Y51_N13
\tx_module_maping|prescaller[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[5]~23_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(5));

-- Location: LCCOMB_X46_Y51_N14
\tx_module_maping|prescaller[6]~25\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[6]~25_combout\ = (\tx_module_maping|prescaller\(6) & (\tx_module_maping|prescaller[5]~24\ $ (GND))) # (!\tx_module_maping|prescaller\(6) & (!\tx_module_maping|prescaller[5]~24\ & VCC))
-- \tx_module_maping|prescaller[6]~26\ = CARRY((\tx_module_maping|prescaller\(6) & !\tx_module_maping|prescaller[5]~24\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \tx_module_maping|prescaller\(6),
	datad => VCC,
	cin => \tx_module_maping|prescaller[5]~24\,
	combout => \tx_module_maping|prescaller[6]~25_combout\,
	cout => \tx_module_maping|prescaller[6]~26\);

-- Location: FF_X46_Y51_N15
\tx_module_maping|prescaller[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[6]~25_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(6));

-- Location: LCCOMB_X46_Y51_N16
\tx_module_maping|prescaller[7]~27\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[7]~27_combout\ = (\tx_module_maping|prescaller\(7) & (!\tx_module_maping|prescaller[6]~26\)) # (!\tx_module_maping|prescaller\(7) & ((\tx_module_maping|prescaller[6]~26\) # (GND)))
-- \tx_module_maping|prescaller[7]~28\ = CARRY((!\tx_module_maping|prescaller[6]~26\) # (!\tx_module_maping|prescaller\(7)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \tx_module_maping|prescaller\(7),
	datad => VCC,
	cin => \tx_module_maping|prescaller[6]~26\,
	combout => \tx_module_maping|prescaller[7]~27_combout\,
	cout => \tx_module_maping|prescaller[7]~28\);

-- Location: FF_X46_Y51_N17
\tx_module_maping|prescaller[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[7]~27_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(7));

-- Location: LCCOMB_X46_Y51_N18
\tx_module_maping|prescaller[8]~29\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[8]~29_combout\ = (\tx_module_maping|prescaller\(8) & (\tx_module_maping|prescaller[7]~28\ $ (GND))) # (!\tx_module_maping|prescaller\(8) & (!\tx_module_maping|prescaller[7]~28\ & VCC))
-- \tx_module_maping|prescaller[8]~30\ = CARRY((\tx_module_maping|prescaller\(8) & !\tx_module_maping|prescaller[7]~28\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \tx_module_maping|prescaller\(8),
	datad => VCC,
	cin => \tx_module_maping|prescaller[7]~28\,
	combout => \tx_module_maping|prescaller[8]~29_combout\,
	cout => \tx_module_maping|prescaller[8]~30\);

-- Location: FF_X46_Y51_N19
\tx_module_maping|prescaller[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[8]~29_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(8));

-- Location: LCCOMB_X46_Y51_N20
\tx_module_maping|prescaller[9]~31\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[9]~31_combout\ = (\tx_module_maping|prescaller\(9) & (!\tx_module_maping|prescaller[8]~30\)) # (!\tx_module_maping|prescaller\(9) & ((\tx_module_maping|prescaller[8]~30\) # (GND)))
-- \tx_module_maping|prescaller[9]~32\ = CARRY((!\tx_module_maping|prescaller[8]~30\) # (!\tx_module_maping|prescaller\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \tx_module_maping|prescaller\(9),
	datad => VCC,
	cin => \tx_module_maping|prescaller[8]~30\,
	combout => \tx_module_maping|prescaller[9]~31_combout\,
	cout => \tx_module_maping|prescaller[9]~32\);

-- Location: FF_X46_Y51_N21
\tx_module_maping|prescaller[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[9]~31_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(9));

-- Location: LCCOMB_X46_Y51_N22
\tx_module_maping|prescaller[10]~33\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[10]~33_combout\ = (\tx_module_maping|prescaller\(10) & (\tx_module_maping|prescaller[9]~32\ $ (GND))) # (!\tx_module_maping|prescaller\(10) & (!\tx_module_maping|prescaller[9]~32\ & VCC))
-- \tx_module_maping|prescaller[10]~34\ = CARRY((\tx_module_maping|prescaller\(10) & !\tx_module_maping|prescaller[9]~32\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(10),
	datad => VCC,
	cin => \tx_module_maping|prescaller[9]~32\,
	combout => \tx_module_maping|prescaller[10]~33_combout\,
	cout => \tx_module_maping|prescaller[10]~34\);

-- Location: FF_X46_Y51_N23
\tx_module_maping|prescaller[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[10]~33_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(10));

-- Location: LCCOMB_X46_Y51_N24
\tx_module_maping|prescaller[11]~35\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[11]~35_combout\ = (\tx_module_maping|prescaller\(11) & (!\tx_module_maping|prescaller[10]~34\)) # (!\tx_module_maping|prescaller\(11) & ((\tx_module_maping|prescaller[10]~34\) # (GND)))
-- \tx_module_maping|prescaller[11]~36\ = CARRY((!\tx_module_maping|prescaller[10]~34\) # (!\tx_module_maping|prescaller\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \tx_module_maping|prescaller\(11),
	datad => VCC,
	cin => \tx_module_maping|prescaller[10]~34\,
	combout => \tx_module_maping|prescaller[11]~35_combout\,
	cout => \tx_module_maping|prescaller[11]~36\);

-- Location: FF_X46_Y51_N25
\tx_module_maping|prescaller[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[11]~35_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(11));

-- Location: LCCOMB_X46_Y51_N26
\tx_module_maping|prescaller[12]~37\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|prescaller[12]~37_combout\ = \tx_module_maping|prescaller\(12) $ (!\tx_module_maping|prescaller[11]~36\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(12),
	cin => \tx_module_maping|prescaller[11]~36\,
	combout => \tx_module_maping|prescaller[12]~37_combout\);

-- Location: FF_X46_Y51_N27
\tx_module_maping|prescaller[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|prescaller[12]~37_combout\,
	sclr => \tx_module_maping|LessThan0~3_combout\,
	ena => \tx_module_maping|f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|prescaller\(12));

-- Location: LCCOMB_X47_Y51_N20
\tx_module_maping|Equal0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Equal0~1_combout\ = (!\tx_module_maping|prescaller\(4) & (!\tx_module_maping|prescaller\(8) & (!\tx_module_maping|prescaller\(7) & \tx_module_maping|prescaller\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(4),
	datab => \tx_module_maping|prescaller\(8),
	datac => \tx_module_maping|prescaller\(7),
	datad => \tx_module_maping|prescaller\(5),
	combout => \tx_module_maping|Equal0~1_combout\);

-- Location: LCCOMB_X47_Y51_N4
\tx_module_maping|Equal0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Equal0~2_combout\ = (!\tx_module_maping|prescaller\(6) & (!\tx_module_maping|prescaller\(10) & (\tx_module_maping|prescaller\(9) & \tx_module_maping|prescaller\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(6),
	datab => \tx_module_maping|prescaller\(10),
	datac => \tx_module_maping|prescaller\(9),
	datad => \tx_module_maping|prescaller\(11),
	combout => \tx_module_maping|Equal0~2_combout\);

-- Location: LCCOMB_X47_Y51_N6
\tx_module_maping|Equal0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Equal0~0_combout\ = (\tx_module_maping|prescaller\(3) & (\tx_module_maping|prescaller\(1) & (\tx_module_maping|prescaller\(2) & \tx_module_maping|prescaller\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(3),
	datab => \tx_module_maping|prescaller\(1),
	datac => \tx_module_maping|prescaller\(2),
	datad => \tx_module_maping|prescaller\(0),
	combout => \tx_module_maping|Equal0~0_combout\);

-- Location: LCCOMB_X47_Y51_N0
\tx_module_maping|Equal0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Equal0~3_combout\ = (!\tx_module_maping|prescaller\(12) & (\tx_module_maping|Equal0~1_combout\ & (\tx_module_maping|Equal0~2_combout\ & \tx_module_maping|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|prescaller\(12),
	datab => \tx_module_maping|Equal0~1_combout\,
	datac => \tx_module_maping|Equal0~2_combout\,
	datad => \tx_module_maping|Equal0~0_combout\,
	combout => \tx_module_maping|Equal0~3_combout\);

-- Location: LCCOMB_X47_Y51_N12
\tx_module_maping|index[1]~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|index[1]~1_combout\ = (\tx_module_maping|f_tx_busy~q\ & \tx_module_maping|Equal0~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \tx_module_maping|f_tx_busy~q\,
	datad => \tx_module_maping|Equal0~3_combout\,
	combout => \tx_module_maping|index[1]~1_combout\);

-- Location: FF_X47_Y51_N19
\tx_module_maping|index[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|index~0_combout\,
	ena => \tx_module_maping|index[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|index\(1));

-- Location: LCCOMB_X47_Y51_N24
\tx_module_maping|index~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|index~3_combout\ = (!\tx_module_maping|index\(3) & (\tx_module_maping|index\(2) $ (((\tx_module_maping|index\(0) & \tx_module_maping|index\(1))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001010001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(3),
	datab => \tx_module_maping|index\(0),
	datac => \tx_module_maping|index\(2),
	datad => \tx_module_maping|index\(1),
	combout => \tx_module_maping|index~3_combout\);

-- Location: FF_X47_Y51_N25
\tx_module_maping|index[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|index~3_combout\,
	ena => \tx_module_maping|index[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|index\(2));

-- Location: LCCOMB_X47_Y51_N30
\tx_module_maping|index~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|index~2_combout\ = (!\tx_module_maping|index\(0) & (((!\tx_module_maping|index\(2) & !\tx_module_maping|index\(1))) # (!\tx_module_maping|index\(3))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010100000111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(3),
	datab => \tx_module_maping|index\(2),
	datac => \tx_module_maping|index\(0),
	datad => \tx_module_maping|index\(1),
	combout => \tx_module_maping|index~2_combout\);

-- Location: FF_X47_Y51_N31
\tx_module_maping|index[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|index~2_combout\,
	ena => \tx_module_maping|index[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|index\(0));

-- Location: LCCOMB_X47_Y51_N22
\tx_module_maping|index~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|index~4_combout\ = (\tx_module_maping|index\(0) & (\tx_module_maping|index\(2) & (!\tx_module_maping|index\(3) & \tx_module_maping|index\(1)))) # (!\tx_module_maping|index\(0) & (!\tx_module_maping|index\(2) & 
-- (\tx_module_maping|index\(3) & !\tx_module_maping|index\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(0),
	datab => \tx_module_maping|index\(2),
	datac => \tx_module_maping|index\(3),
	datad => \tx_module_maping|index\(1),
	combout => \tx_module_maping|index~4_combout\);

-- Location: FF_X47_Y51_N23
\tx_module_maping|index[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|index~4_combout\,
	ena => \tx_module_maping|index[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|index\(3));

-- Location: LCCOMB_X47_Y51_N26
\tx_module_maping|f_tx_busy~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|f_tx_busy~0_combout\ = ((!\tx_module_maping|index\(2) & (!\tx_module_maping|index\(0) & !\tx_module_maping|index\(1)))) # (!\tx_module_maping|index\(3))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010101010111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(3),
	datab => \tx_module_maping|index\(2),
	datac => \tx_module_maping|index\(0),
	datad => \tx_module_maping|index\(1),
	combout => \tx_module_maping|f_tx_busy~0_combout\);

-- Location: LCCOMB_X47_Y51_N8
\tx_module_maping|f_tx_busy~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|f_tx_busy~1_combout\ = (\tx_module_maping|f_tx_busy~0_combout\) # (!\tx_module_maping|Equal0~3_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|f_tx_busy~0_combout\,
	datad => \tx_module_maping|Equal0~3_combout\,
	combout => \tx_module_maping|f_tx_busy~1_combout\);

-- Location: IOIBUF_X46_Y54_N29
\KEY[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: IOIBUF_X54_Y54_N22
\SW[4]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: LCCOMB_X51_Y51_N14
\s_tx_data[4]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[4]~feeder_combout\ = \SW[4]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[4]~input_o\,
	combout => \s_tx_data[4]~feeder_combout\);

-- Location: FF_X51_Y51_N15
\s_tx_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[4]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(4));

-- Location: LCCOMB_X51_Y51_N30
\tx_module_maping|s_tx_data[5]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|s_tx_data[5]~feeder_combout\ = s_tx_data(4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_tx_data(4),
	combout => \tx_module_maping|s_tx_data[5]~feeder_combout\);

-- Location: LCCOMB_X51_Y51_N4
\tx_module_maping|tx_process~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|tx_process~0_combout\ = (!\tx_module_maping|f_tx_busy~q\ & \s_en_tx~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|f_tx_busy~q\,
	datad => \s_en_tx~q\,
	combout => \tx_module_maping|tx_process~0_combout\);

-- Location: FF_X51_Y51_N31
\tx_module_maping|s_tx_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|s_tx_data[5]~feeder_combout\,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(5));

-- Location: IOIBUF_X54_Y54_N29
\SW[3]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: LCCOMB_X50_Y51_N16
\s_tx_data[3]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[3]~feeder_combout\ = \SW[3]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[3]~input_o\,
	combout => \s_tx_data[3]~feeder_combout\);

-- Location: FF_X50_Y51_N17
\s_tx_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[3]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(3));

-- Location: FF_X51_Y51_N11
\tx_module_maping|s_tx_data[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	asdata => s_tx_data(3),
	sload => VCC,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(4));

-- Location: LCCOMB_X51_Y51_N10
\tx_module_maping|Mux0~3\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Mux0~3_combout\ = (\tx_module_maping|index\(1) & (((\tx_module_maping|index\(0))))) # (!\tx_module_maping|index\(1) & ((\tx_module_maping|index\(0) & (\tx_module_maping|s_tx_data\(5))) # (!\tx_module_maping|index\(0) & 
-- ((\tx_module_maping|s_tx_data\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|s_tx_data\(5),
	datab => \tx_module_maping|index\(1),
	datac => \tx_module_maping|s_tx_data\(4),
	datad => \tx_module_maping|index\(0),
	combout => \tx_module_maping|Mux0~3_combout\);

-- Location: IOIBUF_X49_Y54_N1
\SW[5]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: LCCOMB_X50_Y51_N14
\s_tx_data[5]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[5]~feeder_combout\ = \SW[5]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[5]~input_o\,
	combout => \s_tx_data[5]~feeder_combout\);

-- Location: FF_X50_Y51_N15
\s_tx_data[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[5]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(5));

-- Location: LCCOMB_X51_Y51_N28
\tx_module_maping|s_tx_data[6]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|s_tx_data[6]~feeder_combout\ = s_tx_data(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => s_tx_data(5),
	combout => \tx_module_maping|s_tx_data[6]~feeder_combout\);

-- Location: FF_X51_Y51_N29
\tx_module_maping|s_tx_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|s_tx_data[6]~feeder_combout\,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(6));

-- Location: IOIBUF_X54_Y54_N15
\SW[6]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: LCCOMB_X51_Y51_N22
\s_tx_data[6]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[6]~feeder_combout\ = \SW[6]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[6]~input_o\,
	combout => \s_tx_data[6]~feeder_combout\);

-- Location: FF_X51_Y51_N23
\s_tx_data[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[6]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(6));

-- Location: FF_X51_Y51_N21
\tx_module_maping|s_tx_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	asdata => s_tx_data(6),
	sload => VCC,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(7));

-- Location: LCCOMB_X51_Y51_N20
\tx_module_maping|Mux0~4\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Mux0~4_combout\ = (\tx_module_maping|Mux0~3_combout\ & (((\tx_module_maping|s_tx_data\(7)) # (!\tx_module_maping|index\(1))))) # (!\tx_module_maping|Mux0~3_combout\ & (\tx_module_maping|s_tx_data\(6) & ((\tx_module_maping|index\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|Mux0~3_combout\,
	datab => \tx_module_maping|s_tx_data\(6),
	datac => \tx_module_maping|s_tx_data\(7),
	datad => \tx_module_maping|index\(1),
	combout => \tx_module_maping|Mux0~4_combout\);

-- Location: IOIBUF_X58_Y54_N29
\SW[7]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: LCCOMB_X51_Y51_N24
\s_tx_data[7]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[7]~feeder_combout\ = \SW[7]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[7]~input_o\,
	combout => \s_tx_data[7]~feeder_combout\);

-- Location: FF_X51_Y51_N25
\s_tx_data[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[7]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(7));

-- Location: LCCOMB_X51_Y51_N16
\tx_module_maping|s_tx_data[8]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|s_tx_data[8]~feeder_combout\ = s_tx_data(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => s_tx_data(7),
	combout => \tx_module_maping|s_tx_data[8]~feeder_combout\);

-- Location: FF_X51_Y51_N17
\tx_module_maping|s_tx_data[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|s_tx_data[8]~feeder_combout\,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(8));

-- Location: IOIBUF_X51_Y54_N29
\SW[0]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: LCCOMB_X50_Y51_N4
\s_tx_data[0]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[0]~feeder_combout\ = \SW[0]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[0]~input_o\,
	combout => \s_tx_data[0]~feeder_combout\);

-- Location: FF_X50_Y51_N5
\s_tx_data[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[0]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(0));

-- Location: FF_X51_Y51_N3
\tx_module_maping|s_tx_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	asdata => s_tx_data(0),
	sload => VCC,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(1));

-- Location: LCCOMB_X51_Y51_N2
\tx_module_maping|Mux0~1\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Mux0~1_combout\ = (\tx_module_maping|index\(3) & ((\tx_module_maping|s_tx_data\(8)) # ((\tx_module_maping|index\(0))))) # (!\tx_module_maping|index\(3) & (((\tx_module_maping|s_tx_data\(1) & \tx_module_maping|index\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(3),
	datab => \tx_module_maping|s_tx_data\(8),
	datac => \tx_module_maping|s_tx_data\(1),
	datad => \tx_module_maping|index\(0),
	combout => \tx_module_maping|Mux0~1_combout\);

-- Location: IOIBUF_X51_Y54_N1
\SW[2]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: LCCOMB_X51_Y51_N26
\s_tx_data[2]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[2]~feeder_combout\ = \SW[2]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[2]~input_o\,
	combout => \s_tx_data[2]~feeder_combout\);

-- Location: FF_X51_Y51_N27
\s_tx_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[2]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(2));

-- Location: LCCOMB_X51_Y51_N12
\tx_module_maping|s_tx_data[3]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|s_tx_data[3]~feeder_combout\ = s_tx_data(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_tx_data(2),
	combout => \tx_module_maping|s_tx_data[3]~feeder_combout\);

-- Location: FF_X51_Y51_N13
\tx_module_maping|s_tx_data[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|s_tx_data[3]~feeder_combout\,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(3));

-- Location: IOIBUF_X51_Y54_N22
\SW[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: LCCOMB_X50_Y51_N6
\s_tx_data[1]~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_tx_data[1]~feeder_combout\ = \SW[1]~input_o\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \SW[1]~input_o\,
	combout => \s_tx_data[1]~feeder_combout\);

-- Location: FF_X50_Y51_N7
\s_tx_data[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_tx_data[1]~feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => s_tx_data(1));

-- Location: FF_X51_Y51_N19
\tx_module_maping|s_tx_data[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	asdata => s_tx_data(1),
	sload => VCC,
	ena => \tx_module_maping|tx_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|s_tx_data\(2));

-- Location: LCCOMB_X51_Y51_N18
\tx_module_maping|Mux0~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Mux0~0_combout\ = (\tx_module_maping|index\(1) & ((\tx_module_maping|index\(0) & (\tx_module_maping|s_tx_data\(3))) # (!\tx_module_maping|index\(0) & ((\tx_module_maping|s_tx_data\(2))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|s_tx_data\(3),
	datab => \tx_module_maping|index\(0),
	datac => \tx_module_maping|s_tx_data\(2),
	datad => \tx_module_maping|index\(1),
	combout => \tx_module_maping|Mux0~0_combout\);

-- Location: LCCOMB_X47_Y51_N16
\tx_module_maping|Mux0~2\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Mux0~2_combout\ = (\tx_module_maping|index\(3) & (!\tx_module_maping|index\(1) & (\tx_module_maping|Mux0~1_combout\))) # (!\tx_module_maping|index\(3) & ((\tx_module_maping|Mux0~0_combout\) # ((!\tx_module_maping|index\(1) & 
-- \tx_module_maping|Mux0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010100110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(3),
	datab => \tx_module_maping|index\(1),
	datac => \tx_module_maping|Mux0~1_combout\,
	datad => \tx_module_maping|Mux0~0_combout\,
	combout => \tx_module_maping|Mux0~2_combout\);

-- Location: LCCOMB_X47_Y51_N10
\tx_module_maping|Mux0~5\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_maping|Mux0~5_combout\ = (\tx_module_maping|index\(2) & (!\tx_module_maping|index\(3) & (\tx_module_maping|Mux0~4_combout\))) # (!\tx_module_maping|index\(2) & (((\tx_module_maping|Mux0~2_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111001101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \tx_module_maping|index\(3),
	datab => \tx_module_maping|index\(2),
	datac => \tx_module_maping|Mux0~4_combout\,
	datad => \tx_module_maping|Mux0~2_combout\,
	combout => \tx_module_maping|Mux0~5_combout\);

-- Location: FF_X47_Y51_N11
\tx_module_maping|o_tx_data\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|Mux0~5_combout\,
	ena => \tx_module_maping|index[1]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|o_tx_data~q\);

-- Location: LCCOMB_X47_Y51_N2
\tx_module_process~0\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \tx_module_process~0_combout\ = (\KEY[0]~input_o\ & !\tx_module_maping|o_tx_data~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \KEY[0]~input_o\,
	datad => \tx_module_maping|o_tx_data~q\,
	combout => \tx_module_process~0_combout\);

-- Location: LCCOMB_X47_Y51_N14
\s_en_tx~feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \s_en_tx~feeder_combout\ = \tx_module_process~0_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \tx_module_process~0_combout\,
	combout => \s_en_tx~feeder_combout\);

-- Location: FF_X47_Y51_N15
s_en_tx : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \s_en_tx~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \s_en_tx~q\);

-- Location: FF_X47_Y51_N9
\tx_module_maping|f_tx_busy\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \tx_module_maping|f_tx_busy~1_combout\,
	asdata => \s_en_tx~q\,
	sload => \tx_module_maping|ALT_INV_f_tx_busy~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \tx_module_maping|f_tx_busy~q\);

-- Location: LCCOMB_X50_Y51_N12
\LEDR[0]~reg0feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \LEDR[0]~reg0feeder_combout\ = s_tx_data(0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_tx_data(0),
	combout => \LEDR[0]~reg0feeder_combout\);

-- Location: FF_X50_Y51_N13
\LEDR[0]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \LEDR[0]~reg0feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[0]~reg0_q\);

-- Location: LCCOMB_X50_Y51_N10
\LEDR[1]~reg0feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \LEDR[1]~reg0feeder_combout\ = s_tx_data(1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => s_tx_data(1),
	combout => \LEDR[1]~reg0feeder_combout\);

-- Location: FF_X50_Y51_N11
\LEDR[1]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \LEDR[1]~reg0feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[1]~reg0_q\);

-- Location: LCCOMB_X51_Y51_N8
\LEDR[2]~reg0feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \LEDR[2]~reg0feeder_combout\ = s_tx_data(2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_tx_data(2),
	combout => \LEDR[2]~reg0feeder_combout\);

-- Location: FF_X51_Y51_N9
\LEDR[2]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \LEDR[2]~reg0feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[2]~reg0_q\);

-- Location: LCCOMB_X50_Y51_N28
\LEDR[3]~reg0feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \LEDR[3]~reg0feeder_combout\ = s_tx_data(3)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => s_tx_data(3),
	combout => \LEDR[3]~reg0feeder_combout\);

-- Location: FF_X50_Y51_N29
\LEDR[3]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \LEDR[3]~reg0feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[3]~reg0_q\);

-- Location: FF_X51_Y51_N5
\LEDR[4]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	asdata => s_tx_data(4),
	sload => VCC,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[4]~reg0_q\);

-- Location: LCCOMB_X50_Y51_N30
\LEDR[5]~reg0feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \LEDR[5]~reg0feeder_combout\ = s_tx_data(5)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_tx_data(5),
	combout => \LEDR[5]~reg0feeder_combout\);

-- Location: FF_X50_Y51_N31
\LEDR[5]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \LEDR[5]~reg0feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[5]~reg0_q\);

-- Location: LCCOMB_X51_Y51_N6
\LEDR[6]~reg0feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \LEDR[6]~reg0feeder_combout\ = s_tx_data(6)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => s_tx_data(6),
	combout => \LEDR[6]~reg0feeder_combout\);

-- Location: FF_X51_Y51_N7
\LEDR[6]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \LEDR[6]~reg0feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[6]~reg0_q\);

-- Location: LCCOMB_X51_Y51_N0
\LEDR[7]~reg0feeder\ : fiftyfivenm_lcell_comb
-- Equation(s):
-- \LEDR[7]~reg0feeder_combout\ = s_tx_data(7)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => s_tx_data(7),
	combout => \LEDR[7]~reg0feeder_combout\);

-- Location: FF_X51_Y51_N1
\LEDR[7]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \i_clk~inputclkctrl_outclk\,
	d => \LEDR[7]~reg0feeder_combout\,
	ena => \tx_module_process~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \LEDR[7]~reg0_q\);

-- Location: IOIBUF_X31_Y0_N1
\i_rst~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_i_rst,
	o => \i_rst~input_o\);

-- Location: IOIBUF_X78_Y34_N8
\i_rx~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_i_rx,
	o => \i_rx~input_o\);

-- Location: IOIBUF_X49_Y54_N29
\KEY[1]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

-- Location: IOIBUF_X56_Y54_N1
\SW[8]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X69_Y54_N1
\SW[9]~input\ : fiftyfivenm_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	listen_to_nsleep_signal => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: UNVM_X0_Y40_N40
\~QUARTUS_CREATED_UNVM~\ : fiftyfivenm_unvm
-- pragma translate_off
GENERIC MAP (
	addr_range1_end_addr => -1,
	addr_range1_offset => -1,
	addr_range2_offset => -1,
	is_compressed_image => "false",
	is_dual_boot => "false",
	is_eram_skip => "false",
	max_ufm_valid_addr => -1,
	max_valid_addr => -1,
	min_ufm_valid_addr => -1,
	min_valid_addr => -1,
	part_name => "quartus_created_unvm",
	reserve_block => "true")
-- pragma translate_on
PORT MAP (
	nosc_ena => \~QUARTUS_CREATED_GND~I_combout\,
	xe_ye => \~QUARTUS_CREATED_GND~I_combout\,
	se => \~QUARTUS_CREATED_GND~I_combout\,
	busy => \~QUARTUS_CREATED_UNVM~~busy\);

-- Location: ADCBLOCK_X43_Y52_N0
\~QUARTUS_CREATED_ADC1~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 1,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC1~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC1~~eoc\);

-- Location: ADCBLOCK_X43_Y51_N0
\~QUARTUS_CREATED_ADC2~\ : fiftyfivenm_adcblock
-- pragma translate_off
GENERIC MAP (
	analog_input_pin_mask => 0,
	clkdiv => 1,
	device_partname_fivechar_prefix => "none",
	is_this_first_or_second_adc => 2,
	prescalar => 0,
	pwd => 1,
	refsel => 0,
	reserve_block => "true",
	testbits => 66,
	tsclkdiv => 1,
	tsclksel => 0)
-- pragma translate_on
PORT MAP (
	soc => \~QUARTUS_CREATED_GND~I_combout\,
	usr_pwd => VCC,
	tsen => \~QUARTUS_CREATED_GND~I_combout\,
	chsel => \~QUARTUS_CREATED_ADC2~_CHSEL_bus\,
	eoc => \~QUARTUS_CREATED_ADC2~~eoc\);

ww_o_tx <= \o_tx~output_o\;

ww_LEDR(0) <= \LEDR[0]~output_o\;

ww_LEDR(1) <= \LEDR[1]~output_o\;

ww_LEDR(2) <= \LEDR[2]~output_o\;

ww_LEDR(3) <= \LEDR[3]~output_o\;

ww_LEDR(4) <= \LEDR[4]~output_o\;

ww_LEDR(5) <= \LEDR[5]~output_o\;

ww_LEDR(6) <= \LEDR[6]~output_o\;

ww_LEDR(7) <= \LEDR[7]~output_o\;

ww_LEDR(8) <= \LEDR[8]~output_o\;

ww_LEDR(9) <= \LEDR[9]~output_o\;
END structure;


