--
--i_clk -- input clock signal
--i_rst -- input reset signal




library ieee;

use ieee.std_logic_1164.all;

entity tx_module	is

port(

	i_clk			:in std_logic;
	i_rst			:in std_logic;
	i_en_tx		:in std_logic;
	i_data_tx	:in std_logic_vector(7 downto 0);
	
	o_tx_data	:out std_logic;
	f_tx_busy	:out std_logic
);

end entity tx_module;

architecture behav of tx_module is
--Prescaller to 9600 baud, 50000000/9600 = 5208
signal prescaller: integer range 0 to 5208 := 0;
signal index		: integer range 0 to 9 := 0;
signal s_tx_data		: std_logic_vector(9 downto 0);
signal s_tx_flag	:std_logic := '0';

begin 

--rst_process:process(i_clk,i_rst) 
--					begin
--						if(rising_edge(i_clk) and i_rst = '1') then
--								o_tx_data <= '0';
--								f_tx_busy <= '0';
--								prescaller <= 0;
--						end if;					
--end process rst_process;

tx_process:process(i_clk) 
					begin	
						if(rising_edge(i_clk)) then
						if(s_tx_flag = '0' and i_en_tx = '1') then
							s_tx_data(0) <= '0';
							s_tx_data(9) <= '1';
							s_tx_data(8 downto 1) <= i_data_tx;
							
							s_tx_flag <= '1';
							f_tx_busy <= '1';
						end if;
						if(s_tx_flag = '1') then 
							if(prescaller < 5207) then
								prescaller <= prescaller + 1;
							else
								prescaller <= 0;
							end if;
							if(prescaller = 2607) then
								o_tx_data <= s_tx_data(index);
									if(index<9) then
										index <= index + 1;
									else
										index <= 0;
										s_tx_flag <= '0';					
										f_tx_busy <= '0';
									end if;
								end if;	
							end if;					
						end if;
					
end process tx_process;


end architecture behav;