library ieee;

use ieee.std_logic_1164.all;


entity uart is

port(
	i_clk			:in std_logic;
	i_rst			:in std_logic;
	i_rx			:in std_logic;
	--i_EN			:in std_logic;
	--i_tx_data	:in std_logic_vector(7 downto 0);
	
	--o_tx_busy	:out std_logic;
	--o_rx_busy	:out std_logic;
	o_tx			:out std_logic;
	
	--Temporary pins
	KEY			:in std_logic_vector(1 downto 0);
	LEDR			:out std_logic_vector(9 downto 0);
	SW				:in std_logic_vector(9 downto 0)
);

end entity uart;


architecture struct of uart is 

signal s_tx_data		:std_logic_vector(7 downto 0);
signal s_tx_busy		:std_logic;
signal s_en_tx			:std_logic;
signal s_f_tx_busy	:std_logic;


component tx_module 
port(

	i_clk			:in std_logic;
	i_rst			:in std_logic;
	i_en_tx		:in std_logic;
	i_data_tx	:in std_logic_vector(7 downto 0);
	
	o_tx_data	:out std_logic;
	f_tx_busy	:out std_logic
);
end component tx_module;


begin

	tx_module_maping: tx_module port map(i_clk, i_rst, s_en_tx, s_tx_data,s_tx_busy,o_tx);
	
	
tx_module_process:process(i_clk)
						begin
							if(rising_edge(i_clk)) then
								if(KEY(0) = '1' and s_tx_busy = '0') then
									s_tx_data <= SW(7 downto 0);
									s_en_tx <= '1';
									LEDR(7 downto 0) <= s_tx_data;
								else 
									s_en_tx <= '0';
								end if;
							end if;
end process tx_module_process;

end architecture struct;